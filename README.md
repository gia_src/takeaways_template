# TAKEAWYAS TEMPLATE #

Tweetable factbox / "Key takeaways" widget for Guardian NextGen article page. 

Example: http://www.theguardian.com/world/2014/may/28/-sp-struggle-housing-americas-mental-health-care-crisis-care

### Set up ###

* Clone Index.html
* Edit sentence & Tweet info for new boxes (see annotated directions)
* Deploy via gdn-cdn --> embed bucket
* Insert into Composer article template and add image/embed weighting of "supporting" to appear in left hand column